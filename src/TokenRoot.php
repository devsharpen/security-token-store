<?php

declare(strict_types=1);

namespace Devsharpen\Security\TokenStore;

abstract class TokenRoot
{
    /**
     * @var array
     */
    private $recordedEvents = [];

    /**
     * @var int
     */
    protected $version = 0;

    protected function recordThat(TokenChanged $event): void
    {
        ++$this->version;

        $this->recordedEvents[] = $event->withVersion($this->version);

        $this->apply($event);
    }

    public function popRecordedEvents(): array
    {
        $pendingEvents = $this->recordedEvents;

        $this->recordedEvents = [];

        return $pendingEvents;
    }

    protected function apply(TokenChanged $e): void
    {
        $handler = $this->determineEventHandlerMethodFor($e);

        if (!method_exists($this, $handler)) {
            throw new \RuntimeException(sprintf(
                'Missing event handler method %s for token root %s',
                $handler,
                get_class($this)
            ));
        }

        $this->{$handler}($e);
    }

    protected function determineEventHandlerMethodFor(TokenChanged $e): string
    {
        return 'when' . implode('', array_slice(explode('\\', get_class($e)), -1));
    }
}