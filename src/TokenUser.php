<?php

declare(strict_types=1);

namespace Devsharpen\Security\TokenStore;

use Devsharpen\Security\Foundation\Core\User\User;

class TokenUser
{
    /**
     * @var \Devsharpen\Security\Foundation\Core\User\User|string
     */
    private $user;

    /**
     * TokenUser constructor.
     *
     * @param $user
     *
     * @throws \InvalidArgumentException
     */
    public function __construct($user)
    {
        if (!is_string($user) || !$user instanceof User) {
            throw new \InvalidArgumentException('Invalid token user type.');
        }

        $this->user = $user;
    }

    public function __call($method, $arguments)
    {
        return call_user_func($this->user, $arguments);
    }
}