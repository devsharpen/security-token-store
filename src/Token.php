<?php

declare(strict_types=1);

namespace Devsharpen\Security\TokenStore;

use Ramsey\Uuid\Uuid;

abstract class Token extends TokenRoot
{
    /**
     * @var bool
     */
    private $authenticated = false;

    /**
     * @var array
     */
    private $roles = [];

    /**
     * @var
     */
    private $user;

    /**
     * Token constructor.
     *
     * @param array $roles
     */
    protected function __construct(array $roles = [])
    {
        $this->roles = $roles;

        count($roles) > 0 and $this->authenticated = true;
    }

    public function isAuthenticated(): bool
    {
        return $this->authenticated;
    }

    public function setUser($user): void
    {
        $this->user = new TokenUser($user);
    }

    public function getUser(): TokenUser
    {
        return $this->user;
    }

    protected function setAuthenticated(bool $authenticated): void
    {
        $this->authenticated = $authenticated;
    }

    public function getRoles(): array
    {
        return $this->roles;
    }

    protected function nextTokenId(): string
    {
        return Uuid::uuid4()->toString();
    }

    abstract protected function newInstance(array $roles = []): self;
}