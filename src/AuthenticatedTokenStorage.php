<?php

declare(strict_types=1);

namespace Devsharpen\Security\TokenStore;

use Devsharpen\Security\TokenStore\Exception\TokenStoreException;

class TokenStorage implements AuthenticationTokenStore
{
    /**
     * @var \Devsharpen\Security\TokenStore\Token
     */
    private $token;

    public function getToken(): ?Token
    {
        return $this->token;
    }

    public function setAuthenticatedToken(Token $token): void
    {
        if (!$token->isAuthenticated()) {
            throw new TokenStoreException('Token is not authenticated.');
        }

        $this->token = $token;
    }

    public function eraseStorage(): void
    {
        $this->token = null;
    }
}