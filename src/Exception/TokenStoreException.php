<?php

declare(strict_types=1);

namespace Devsharpen\Security\TokenStore\Exception;

class TokenStoreException extends \RuntimeException
{
}