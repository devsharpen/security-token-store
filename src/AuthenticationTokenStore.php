<?php

declare(strict_types=1);

namespace Devsharpen\Security\TokenStore;

interface AuthenticationTokenStore
{
    public function getToken(): ?Token;

    public function setAuthenticatedToken(Token $token): void;
}