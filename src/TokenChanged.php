<?php

declare(strict_types=1);

namespace Devsharpen\Security\TokenStore;

use Assert\Assertion;
use Devsharpen\Security\Common\Messaging\TokenEvent;

abstract class TokenChanged extends TokenEvent
{
    /**
     * @var array
     */
    protected $payload = [];

    public static function occur(string $tokenId, array $payload = []): self
    {
        return new static($tokenId, $payload);
    }

    protected function __construct(string $tokenId, array $payload, array $metadata = [])
    {
        //Metadata needs to be set before setTokenId and setVersion is called
        $this->metadata = $metadata;
        $this->setTokenId($tokenId);
        $this->setVersion($metadata['_token_version'] ?? 1);
        $this->setPayload($payload);
        $this->init();
    }

    public function tokenId(): string
    {
        return $this->metadata['_token_id'];
    }

    public function payload(): array
    {
        return $this->payload;
    }

    public function version(): int
    {
        return $this->metadata['_token_version'];
    }

    public function withVersion(int $version): TokenChanged
    {
        $self = clone $this;
        $self->setVersion($version);

        return $self;
    }

    protected function setTokenId(string $tokenId): void
    {
        Assertion::notEmpty($tokenId);

        $this->metadata['_token_id'] = $tokenId;
    }

    protected function setVersion(int $version): void
    {
        $this->metadata['_token_version'] = $version;
    }

    protected function setPayload(array $payload): void
    {
        $this->payload = $payload;
    }
}