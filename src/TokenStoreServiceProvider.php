<?php

declare(strict_types=1);

namespace Devsharpen\Security\TokenStore;

use Illuminate\Support\ServiceProvider;

class TokenStoreServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->singleton(AuthenticationTokenStore::class, TokenStorage::class);
    }
}